import { connect } from 'react-redux'
import { setVisibilityFilter } from '../actions'
import Link from '../components/Link'

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.visibilityFilter
  }
}
//mapStateToProps() -> 轉換目前 Redux store state 成為props，傳到你要傳的component


const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      dispatch(setVisibilityFilter(ownProps.filter))
      //setVisibilityFilter() -> action
    }
  }
}
//mapDispatchToProps() -> 它接收 dispatch() method 並回傳你想要注入 component 的 callback props (function)

const FilterLink = connect(
  mapStateToProps,
  mapDispatchToProps
)(Link)
//connect() -> 建立FilterLink container,傳入mapStateToProps()、mapDispatchToProps()這兩個function,傳給Link component

export default FilterLink

//基礎的container用法
