import React from 'react'
import Footer from './Footer'
import AddTodo from '../containers/AddTodo'
import VisibleTodoList from '../containers/VisibleTodoList'
import ActivityInfoContainer from '../activityInfo/ActivityInfoContainer.js'

const App = () => (
  <div>
    <AddTodo />
    <VisibleTodoList />
    <Footer />
    <ActivityInfoContainer />
  </div>
)
//AddTodo,VisibleTodoList,ActivityInfo -> container
//Footer -> component

export default App


//export default class App extends React.Component { }  <- 我們現在建立react component的方式，並且export


//export default class App extends React.Component {
//  render(){
//    return(
//        <div>
//          <AddTodo />
//          <VisibleTodoList />
//          <Footer />
//        </div>
//    )
//  }
//}    <- 我們現在render的寫法
