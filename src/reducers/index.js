import { combineReducers } from 'redux'
import todos from './todos'
import visibilityFilter from './visibilityFilter'
import activityInfo from '../activityInfo/ActivityInfoReducer.js'

const todoApp = combineReducers({
  todos,
  visibilityFilter,
  activityInfo
})

//combineReducers() 結合每個小的reducer，合成root reducer

export default todoApp
