import { connect } from 'react-redux'
import {getActivityInfo} from './ActivityInfoAction.js'
import ActivityInfo from './ActivityInfoComponent.js'

const mapStateToProps = (state, ownProps) => {
    return {
        activityInfo: state.activityInfo
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onClick: () => {
            dispatch(getActivityInfo(149,964890790261256))
        }
    }
}

const ActivityInfoContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ActivityInfo)

export default ActivityInfoContainer
