import React, { PropTypes } from 'react'

const ActivityInfo = ({activityInfo, onClick }) => {
    return(
        <div>
            <a href="#" onClick={e => {onClick()}}>
                getActivityInfo
            </a>
            <div>
                {activityInfo.description}
            </div>
        </div>
    )
}

ActivityInfo.propTypes = {
    activityInfo: PropTypes.any.isRequired,
    onClick: PropTypes.func.isRequired
}

export default ActivityInfo
